# Real time data
Follow 2 step below to retreiving reatime data of EBus station (ex:239-ebus-station)
## Step 1: Get Accept Token
- This is the rootCA of our platform: [ca-crt](/keys/ca-crt.pem).
- Access our platform to add thing or download key folowing :
  + 23/9 Park Bus Station
        - [client-239-crt](/keys/client-239-crt.pem)
        - [client-239-key](/keys/client-239-key.pem)
        - serial: "8e8e4d60-049c-11e8-8bae-11b86d009c56"
        - name: "239-ebus-station"
  + ChoLonA Bus Station
        - [client-choLonA-crt](/keys/client-choLonA-crt.pem)
        - [client-choLonA-key](/keys/client-choLonA-key.pem)
        - serial: "bbbdfd80-049c-11e8-8bae-11b86d009c56"
        - name: "choLonA-ebus-station"
  + ChoLonB Bus Station
        - [client-choLonB-crt](/keys/client-choLonB-crt.pem)
        - [client-choLonB-key](/keys/client-choLonB-key.pem)
        - serial: "f4770450-049c-11e8-8bae-11b86d009c56"
        - name: "choLonB-ebus-station"
 + Cau Ong Lanh Bus Station
        - [client-cauOngLanh-crt](/keys/client-cauOngLanh-crt.pem)
        - [client-cauOngLanh-key](/keys/client-cauOngLanh-key.pem)
        - serial: "3867b040-28f8-11e8-8fae-43841b6e8b32"
        - name: "COL-ebus-station"
 + Nguyen Thi Nghia Bus Station
        - [client-nguyenThiNghia-crt](/keys/client-nguyenThiNghia-crt.pem)
        - [client-nguyenThiNghia-key](/keys/client-nguyenThiNghia-key.pem)
        - serial: "32d1d0f0-28f5-11e8-8fae-43841b6e8b32"
        - name: "NTN-ebus-station"
 + Duc Ba Bus Station
        - [client-ducBa-crt](/keys/client-ducBa-crt.pem)
        - [client-ducBa-key](/keys/client-ducBa-key.pem)
        - serial: "8c0c6470-43c9-11e8-a2bb-b9abfe3e8de8"
        - name: "DucBa-ebus-station"
 + Diamond Plaza Bus Station
        - [client-diamondPlaza-crt](/keys/client-diamondPlaza-crt.pem)
        - [client-diamondPlaza-key](/keys/client-diamondPlaza-key.pem)
        - serial: "cd6c6280-43c9-11e8-a2bb-b9abfe3e8de8"
        - name: "DiamondPlaza-ebus-station"
  
- Use 3 keys: rooCA, crt and private key to connect our broker MQTT:
  + Example of 23/9 Park Bus Station:
        * PORT = 8883;
        * HOST = ‘mqtt.319mn.com';
        * protocol: mqtts;
        * username:"239-ebus-station";
        * password:"8e8e4d60-049c-11e8-8bae-11b86d009c56";
        * clientId:"8e8e4d60-049c-11e8-8bae-11b86d009c56".

After connect successful, you will receive a thingToken in topic things/239-ebus-station/clientToken.

 => This is the accept token.

## Step 2: Get realtime data
- Access our swagger in case of you don't know how to test api: 
    - http://iot-api.319mn.com/api-docs/
- After having accept token, you can get real time data of 239-ebus-station following api:
>http://iot-api.319mn.com/things/239-ebus-station/properties/realtime-data/status

- Remember input accept token before make a request:
![Image of realtime_detail](/image-help/input-accept-token.png).
- Data receiving description:
    - arrs: .
      - d: Distance from this station to the bus.
      - dts: Timestamp of data.
      - s: Speed of bus.
      - sts: .
      - t: Estimate arriving time(s).
      - v: Plate number.

    - r: Route id.
    - rN: Route name (in Vietnamese).
    - rNo: Route number.
    - s: Station id.
    - sN: Bus station names (in Vietnamese).
    - v: .
    - vN: Station name end path.
    - snEn: Bus station names (in English).
    - rnEn: Route name (in English).
    
    ### Note: if t or dts has value -1, this ebus is central station and only display departure time on 24" inch ( not show distance)

![Image of realtime_detail](/img/realtime_detail.png).