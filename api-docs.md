# Use api-docs

- Access to http://iot-api.319mn.com/api-docs.
- Then, go to Account> Token,then type user: info@319mn.com and password: 123456 to get accept token.
- ![Image of accept token](/img/accept_token.PNG).
- Then paste accept token to here.![Image of paste accept token](/img/paste_accept_token.PNG)
- Final step, go to ThingProperties> GET /things/{thingName}/properties/{propertyName}/status, type thingName and propertyName. Then click "Try it now".
- For get all bus route: 
 + thingName = hcm-bus-data .
 + propertyName = route .
- For get all stop( station):
 + thingName = hcm-bus-data .
 + propertyName = station .


# Get all Bus Routes

> http://iot-api.319mn.com/things/hcm-bus-data/properties/route/status

- RouteId( integer): Id of the route.
- RouteNo( string): Route number.
- RouteName(string): Name of the route.
- Type(string): Type of the route.
- Distance(integer): Distance of the route, from the first bus stops to the end bus stops.
- TimeTableIn(string): List of the time table of the route forward trip( measured in seconds), each time time of a trip split by comma. Example: TimeTableIn :"18000-19800,18600-20400" mean the route have 2 buses. The first bus start from 5:00 PM to 6:30 PM and the second bus start from  5:10 PM to 6:40 PM.
- TimeTableOut(string):List of the time table of the route backward trip( measured in seconds). The same as TimeTableIn.
- SpeedIn(integer):
- Tickets(string): Prices of tickets.
- TotalTrip(string): Number of trips per a day. Example the Route 01 has TotalTrip is 240 trips/day
- TimeOfTrip: Route time. Is it the average times a bus spend on a forward trip or backward trip. Example the Route 01 has TimeOfTrip is 30 - 35 minutes.
- Orgs(string): Orgnization
- OutDesc(string): List of bus stops name on route trip backward.
- InDesc(string):List of bus stops name on route trip forward.
- Headway(string): Bettwen two bus.


# Get all Bus Stops

> http://iot-api.319mn.com/things/hcm-bus-data/properties/station/status

- StationId(integer): Id of the bus stops.
- StationOrder(integer): bus stops order.
- StationDirection(integer): 
- RouteId(integer): Id of the route.
- RouteVarId(integer): id of the route var.
- StationCode(string): bus stops code.
- StationName(string): name of the bus stops.
- pathPoints(string):
- Address(integer): Address of the bus stops.
- Lat(integer): Latitude.
- Lng(integer): Longitude.
- K(integer): K.