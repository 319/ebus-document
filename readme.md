# Ebus documentation
## Feature
- [Search for places](#home-page).
- [Directions](#direction-page).
- [Lookup](#lookup-page).

## Who's use
- For every people. Mostly students, foreigner, tourist.

## Documentation

#### General
> General item.

![Image of Menu](/img/menu.png)

- When touch ![Image of Menu button](/img/menu-button.png), menu side will appear in the left panel of screen.
 + Language setting: show popup change language to Vietnamese or English, button close popup.
 + Feedback: Navigate feedback page. Feedback page include email( require), phone number( option), content( require), button submit, button cancel to go back, button (x) to go back.
 + About: show popup information of the application, button Close to go back, button (x) to go back.

- When touch ![Image of Current location](/img/your-location.png), your location will be selected on the map. 

- When touch ![Image of expand-button](/img/expand-button.png), lookup button ![Image of lookup-button](/img/lookup-button.png) and direction ![Image of finding-route-button](/img/finding-route-button.png) will be show/hide.

- ![Image of lookup-button](/img/lookup-button.png), switch to lookup page.

- ![Image of finding-route-button](/img/finding-route-button.png), switch to finding a route page.

- ![Image of textbox](/img/textbox.png), autocomplete location map. Use field Address of api get stations to suggest data.
- When touch ![Image of bus](/img/bus.png), popup bus stop detail will be appear .

- ![Image of bus-stop-detail](/img/bus-stop-detail.png), popup bus stop detail include:
 + Information of bus stop. Use api get all bus stop to get bus stop information.
 + Available buses come over. Use RouteId to find RouteNo in api get all bus routes.

#### Home page:

> To search for places.
![Image of Homepage](/Home screen-EN.png)

- To search a place on the map. When a place is found, the map will show it in center map.
- Search for places is home page. After user type a place in textbox "search  for places", textbox will suggest some place map with characters user typed( data get from local map address). 
![Image of direction-text-input-page](img/direction-text-input-page.png)

Then user click a place in list place suggestion, the map will show this place on center map.

#### Direction page:

> To get some direction routes.
![Image of DirectionEN](/Direction-EN.png)

- When user touch ![Image of finding-route-button](/img/finding-route-button.png), navigate to finding route page.

![Image of direction-1](/img/direction-1.png)
- When user touch ![Image of your-location2](/img/your-location2.png), current location will be chosen.
- When user touch input your departure or destination for typing a place, an autocomplete list location suggestion will be show below textbox. See image below:
![Image of direction-text-input-page](/img/direction-text-input-page.png)

- ![Image of number-route](/img/number-route.png),it is a dropdown with value 1,2,3,etc.
- Currently working route only: Condition for finding a direction. ON is only looking for direction by bus is running. OFF is else.
- Button close and button (x) to close direction popup. Click ![Image of finding-route-button](/img/finding-route-button.png) to open popup again.
- Button direction ![Image of direction-button](/img/direction-button.png) will enable when departure and destination are selected. Button direction is submit finding routes.
If search result has multiple direction: 
![Image of direction-multiple-direction](/img/direction-multiple-direction.png)
- Touch a row to select a direction
![Image of direction-3](/img/direction-3.png).
- Touch (6) to open direction detail ![Image of direction-4](/img/direction-4.png).
- Touch (7) or instruction to go to ![Image of direction-5](/img/direction-5.png). 
- Touch (8) or station to go back.
#### Lookup page:

> To lookup a route
![Image of Lookup](/Lookup-EN.png) 

- When user touch Lookup button ![Image of lookup-button](/img/lookup-button.png), screen will show list of direction:
 ![Image of lookup-1](/img/lookup-1.png)
- Select a route, screen will show the direction for this route:
![Image of lookup-1_1](/img/lookup-1_1.png).
- Touch (6), screen will show route detail:
![Image of lookup-1_2](/img/lookup-1_2.png).
- Touch (7) or down arrow to hide direction detail.

#### Available for offline mode.
> Bus map application have to has Offline mode feature. 

### API to get data of all routes
- Please go to [api documentation](/api-docs.md)

### How to get realtime data
- Please go to [realtime documentation](/realtime-doc.md)